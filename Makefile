all:
	make -C ESP32/
	make -C Teensy/

upload:
	make -C ESP32/ uploadfs
	make -C ESP32/ upload
	make -C Teensy/ upload
