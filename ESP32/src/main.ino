///LIBRARIES
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include <Ticker.h>
#include <SPI.h>
#include <Ucglib.h>
#include <menu.h>
#include <menuIO/UCGLibOut.h>
#include <menuIO/chainStream.h>
#include <ClickEncoder.h>
#include <menuIO/clickEncoderIn.h>
#include <menuIO/keyIn.h>
#include <SPIFFS.h>
#include <EasyTransfer.h>
#include <CmdParser.hpp>

///HEADERS
#include "DebugUtils.h"
#include "disp_macros.h"
#include "menu_def.h"
#include "pin_def.h"
#include "config.h"

//MACROS
/*#define DEBUG*/
/*#define SERIALCMD*/

// CLASSES
AsyncWebServer server(80);
AsyncWebSocket ws("/ws");
Ticker displayTicker;
Ticker debugMsgTicker;
EasyTransfer ETin, ETout;
HardwareSerial SerialTeensy(2);
CmdParser cmdParser;
//SPIClass SPI2(HSPI);        //This is for the SD-Card, not implemented in v1.0

//UART STRUCTS
struct SEND_DATA_STRUCTURE {
    int targetSpeed;
    int targetPosition;
    int targetPressure;
    char cmd;
};

struct RECEIVE_DATA_STRUCTURE {
    float force;
    long currentPosition;
    unsigned samples;
    char state;
};

RECEIVE_DATA_STRUCTURE rxdata;
SEND_DATA_STRUCTURE txdata;

//GLOBAL VARIABLES
byte prevSec;
char prevState;
long prevPosition;
float prevForce;
long prevSampleCount;

void sendDataWs()
{
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    root["t"] = millis();
    root["p"] = flowrate;
    root["s"] = rxdata.force;
    size_t len = root.measureLength();
    AsyncWebSocketMessageBuffer * buffer = ws.makeBuffer(len);
    if (buffer) {
        root.printTo((char *)buffer->get(), len + 1);
        ws.textAll(buffer);
    }
}

char *statusString(char statusChar)
{
    char * buf = (char *) malloc (18);
    switch(statusChar) {
    case 'i':
        strcpy(buf, "Standby");
        break;
    case 'k':
        strcpy(buf, "Homing");
        break;
    case 'o':
        strcpy(buf, "Homed");
        break;
    case 'h':
        strcpy(buf, "Stopped");
        break;
    case 'e':
        strcpy(buf, "Extruding");
        break;
    case 'p':
        strcpy(buf, "Pumping");
        break;
    case 'c':
        strcpy(buf, "Calibration");
        break;
    default:
        strcpy(buf, &statusChar);
        break;
    }
    return buf;
}

char *charArrayIP(int ip)
{
    char * buf = (char *) malloc (18);
    unsigned char bytes[4];
    bytes[0] = ip & 0xFF;
    bytes[1] = (ip >> 8) & 0xFF;
    bytes[2] = (ip >> 16) & 0xFF;
    bytes[3] = (ip >> 24) & 0xFF;
    sprintf(buf, "%d.%d.%d.%d", bytes[0], bytes[1], bytes[2], bytes[3]);
    return buf;
}

#define COL1 5
#define COL2 170
#define COL3 260
#define ROW1 165
#define ROW2 188
#define ROW3 211
#define ROW4 234


void displayStatus()
{
    if (prevState != rxdata.state) {
        char *stat_p =  statusString(rxdata.state);
        ucg.setPrintPos(COL1, ROW1);
        ucg.printf("Status:%s %*s", stat_p, 11 - strlen(stat_p), "");   //print status string and trailing whitespace
        free(stat_p);
        prevState = rxdata.state;
    }
}

void displayUptime()
{
    unsigned long now = millis();
    byte minute = (now / 60000);
    now -= (minute * 60000);
    byte second = (now / 1000);
    if (second != prevSec) {
        ucg.setPrintPos(COL1, ROW2);
        ucg.printf("Up time: %02d:%02d", minute, second);
        prevSec = second;
    }
}

void displayPumpParam()
{
    if (newPumpParam) {
        ucg.setPrintPos(COL1, ROW3);
        ucg.printf("Dose %.2f ml", dose);
        ucg.setPrintPos(COL2 - 8, ROW3);
        ucg.printf("Rate %.2f ml/min", flowrate);
        newPumpParam = false;
    }
}

void displaySampleCount()
{
    ucg.setPrintPos(COL2, ROW2);    //We are retrieving values faster than the display updates, so we can update this on every refresh
    ucg.printf("Samples: %d", rxdata.samples );
}

void displayForce()
{
    if (rxdata.force != prevForce) {
        ucg.setPrintPos(COL1, ROW4);
        ucg.printf("Force: %.2f", rxdata.force);
        prevForce = rxdata.force;
    }
}

void displayPosition()
{
    if (rxdata.currentPosition != prevPosition) {
        ucg.setPrintPos(COL2, ROW4);
        ucg.printf("Position: %05d", rxdata.currentPosition);
        prevPosition = rxdata.currentPosition;
    }
}


void updateDisplay()
{
    nav.poll();
    ucg.setFontMode(UCG_FONT_MODE_SOLID);
    displayStatus();
    //COL2, ROW1 is for IP which is set in setup since it is unlikely to change
    displayUptime();
    displayPumpParam();
    displaySampleCount();
    displayForce();
    displayPosition();
    ucg.setFontMode(UCG_FONT_MODE_TRANSPARENT);
}

void processSerial()
{
    if (Serial.available()) {
        CmdBuffer<32> myBuffer;
        float newDose;
        float newRate;
        if (myBuffer.readFromSerial(&Serial, 3000)) {
            if (cmdParser.parseCmd(&myBuffer) != CMDPARSER_ERROR) {
                if (strcmp ("Dose", cmdParser.getCommand()) == 0) {
                    newPumpParam = true;
                    newDose = atof (cmdParser.getValueFromKey("Dose"));
                    if (newDose > 0 && newDose <= 20) {
                        dose = newDose;
                        DEBUG_PRINTF("Changing dose to %.2f ml \n", dose);
                    }
                    else {
                        DEBUG_PRINTLN("Dose out of bounds");
                    }
                }
                else if (strcmp ("Rate", cmdParser.getCommand()) == 0) {
                    newPumpParam = true;
                    newRate = atof (cmdParser.getValueFromKey("Rate"));
                    if (newRate > 0 && newRate <= 20) {
                        flowrate = newRate;
                        DEBUG_PRINTF("Changing flowrate to %.2f ml/min \n", flowrate);
                    }
                    else {
                        DEBUG_PRINTLN("Flowrate out of bounds");
                    }
                }
                else if (strcmp ("Run", cmdParser.getCommand()) == 0) {
                    DEBUG_PRINTLN("Moving...");
                    menuCMD = 'p';
                    newCMD = true;
                }
                else if (strcmp ("Meas", cmdParser.getCommand()) == 0) {
                    ETin.receiveData();                             //Make sure we have fresh values
                    Serial.printf("%d\t%d\t%d\t%.2f\t%c\n", millis(), rxdata.samples, rxdata.currentPosition, rxdata.force, rxdata.state);
                }
                else if (strcmp ("Home", cmdParser.getCommand()) == 0) {
                    DEBUG_PRINTLN("Homing...");
                    menuCMD = 'k';
                    newCMD = true;
                }
            }
            else {
                Serial.println("Parser error!");
            }
        }
        else {
            Serial.println("TIMEOUT!");
        }
    }
}

void printDebug()
{
    DEBUG_PRINTF("STATE %c\n\r", rxdata.state);
    DEBUG_PRINTF("CMD %c\n\r", txdata.cmd);
    DEBUG_PRINTF("SPEED %d\n\r", flowrate);
    DEBUG_PRINTF("Force %.2f\n\r", rxdata.force);
}

void SocketTask( void * pvParameters )
{
    for(;;) {
        sendDataWs();
        //Only send command if new command or if previous pump done
        if (newCMD) {
            DEBUG_PRINTLN("Sending new command");
            txdata.cmd = menuCMD;
            txdata.targetPosition = dose * ML2STEPS;        //ML2STEPS macro defined in configuration
            txdata.targetSpeed = flowrate * ML2STEPS / 60;
            ETout.sendData();
            newCMD = false;
        }
        delay(100);         //Run this loop every 100 ms
    }
}

void setup()
{
    Serial.begin(115200);
    DEBUG_PRINTLN("debug Mode");
    SerialTeensy.begin(115200);
    SPIFFS.begin();
    ETin.begin(details(rxdata), &SerialTeensy);
    ETout.begin(details(txdata), &SerialTeensy);
    ucg.begin(UCG_FONT_MODE_TRANSPARENT);
    ucg.clearScreen();
    ucg.setFont(ucg_font_helvR14_hf);       //choose fized width font (monometric)
    ucg.setFontPosTop();
    ucg.setRotate90();
    ucg.setFontPosBottom();
#ifdef SERIALCMD
    ucg.setPrintPos(60, 120);
    ucg.print("Serial command mode!");
#else
    pinMode(ENCODER_BTN, INPUT_PULLUP);
    pinMode(ENCODER_DT, INPUT_PULLUP);
    pinMode(ENCODER_CLK, INPUT_PULLUP);
    if (AP_MODE) {
        WiFi.softAP(ssid, password);
        ucg.setPrintPos(60, 140);
        ucg.printf("AP mode, connect to: %s", ssid);
        delay (1000);
        ucg.clearScreen();
        ucg.setPrintPos(10, 178);
        ucg.print("AP mode ");
    }
    else {
        ucg.setPrintPos(60, 120);
        ucg.printf("Connecting to %s", ssid);
        ucg.setPrintPos(60, 120);
        WiFi.begin(ssid, password);
        for (int attempts = 0; attempts < 10; attempts ++) {
            if(WiFi.status() != WL_CONNECTED) {
                DEBUG_PRINT(".");
                delay(200);
            }
        }
        ucg.setPrintPos(60, 140);
        if(WiFi.status() != WL_CONNECTED) ucg.print("Couldn't connect");
        else ucg.print("Connected!");
        delay(500);
        ucg.clearScreen();
    }
    server.serveStatic("/", SPIFFS, "/").setDefaultFile("index.html");
    server.addHandler(&ws);
    server.begin();
    ucg.setColor(255, 255, 255);
    ucg.drawFrame(0, 142, 320, 98);        //Draw frame around dynamic values
    ucg.setPrintPos(COL2, ROW1);
    char *ip_p =  charArrayIP(WiFi.localIP());
    DEBUG_PRINTLN(WiFi.localIP());
    ucg.printf("IP:%s", ip_p);
    free(ip_p);
    displayTicker.attach_ms(500, updateDisplay);    //refresh rate of 2 Hz, if increased results in resets becaused of wdt
    debugMsgTicker.attach_ms(1000, printDebug);
#endif
    xTaskCreatePinnedToCore(
        SocketTask,     //  [> Function to implement the task <]
        "SocketTask",       //  [> Name of the task <]
        4096,       //  [> Stack size in words <]
        NULL,       //  [> Task input parameter <]
        2,          //  [> Priority of the task <]
        NULL,       //  [> Task handle. <]
        1); //      [> Core where the task should run <]
}


void loop()
{
    ETin.receiveData();
#ifdef SERIALCMD
    processSerial();
    displayPumpParam();
#else
    encoder.service();
#endif
    if (resetFlag) ESP.restart();
}
