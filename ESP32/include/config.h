#pragma once


//=====STEPPER=====//
#define STEPS_PER_REVOLUTION 200
#define MICRO_STEPS  16
#define LEAD_SCREW_PITCH 2

//=====SYRINGE=====//
#define BODY_RADIUS 8
// Pitch of stepper motor : 0.794mm 
// 1/16th microstepping
// 1 revolution is 200*16 steps so one 1mm is 4030.227 steps 
// with a syringe diameter of 16 mm, 1 mm corresponds to pi*8^2*1 mm^3 = 0.20106 ml 
// so 1 ml is 4.9736 mm which is 20044.70465 steps
#define ML2STEPS 1000*STEPS_PER_REVOLUTION*MICRO_STEPS/(LEAD_SCREW_PITCH*PI*BODY_RADIUS*BODY_RADIUS)

//=====LOAD CELL AMPLIFIER=====//
#define OFFSET
#define SCALE 

//=====NETWORK=====//
#define AP_MODE 0

#if (AP_MODE)
    const char* ssid = "Ystruder";
    const char* password =  "ystruder";
#else
    const char* ssid = "aalto open";
    const char* password =  "";
#endif


