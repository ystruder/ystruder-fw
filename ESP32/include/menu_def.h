#include "pin_def.h"

ClickEncoder encoder = ClickEncoder(ENCODER_DT,ENCODER_CLK,ENCODER_BTN,ENCODER_STEPS_PER_NOTCH);
ClickEncoderStream encStream(encoder,1);
Ucglib_ILI9341_18x240x320_HWSPI ucg(UC_DC , UC_CS, UC_RST);

int mode=0;
float dose = 10;                // dose in ml
float flowrate= 20;             // flowrate in ml/min
float pressureLimit=9;          // pressure limit in bar
extern char menuCMD = 'i';
extern bool newCMD = false;
extern bool newPumpParam = true;
extern bool resetFlag = false;
using namespace Menu;

////Pump 
result pumpFun(){ 
    menuCMD= 'p'; 
    newCMD = true;
    DEBUG_PRINTLN("PUMP FUN");
    return proceed;
}

result pumpParam(){                     //This functions runs every time the pump parameters change. When parameters change the fields are updated.
    DEBUG_PRINTLN("NEW PUMP PARAM");
    newPumpParam = true;
    return proceed;
}

MENU(pumpMenu,">Pump menu",doNothing,noEvent,wrapStyle
        ,FIELD(flowrate,"Flow rate"," ml/min",0,20,1,0.1,pumpParam,enterEvent,noStyle)
        ,FIELD(dose,"Dose"," ml",0,12,1,0.1,pumpParam,enterEvent,noStyle)
        ,FIELD(pressureLimit,"Pressure limit"," bar",0,10,1,0.1,pumpParam,enterEvent,noStyle)
        ,OP("Pump", pumpFun, enterEvent)
        ,EXIT("<Back")
    );

////Extruder 
result extrFun(){ 
    menuCMD= 'e'; 
    newCMD = true;
    DEBUG_PRINTLN("EXTRUDER_CMD");
    return proceed;
}

SELECT(mode,extruderMode,"Mode: ",doNothing,noEvent,noStyle
        ,VALUE("Continuous",0,doNothing,noEvent)
        ,VALUE("Constant pressure",1,doNothing,noEvent)
        ,VALUE("Constant speed",2,doNothing,noEvent)
      );

MENU(extruderMenu,">Extruder menu",doNothing,noEvent,wrapStyle
        ,OP("Extrude", extrFun, enterEvent)
        ,SUBMENU(extruderMode)
        ,EXIT("<Back")
    );

/////Main menu
result resetFun(){ 
    resetFlag = true;
    return proceed;
}

result calibFun(){ 
    menuCMD= 'c'; 
    newCMD = true;
    DEBUG_PRINTLN("calibration function");
    return proceed;
}

MENU(settingsMenu,">Settings",doNothing,noEvent,wrapStyle
        ,OP("Calibrate", calibFun, enterEvent)
        ,OP("Reset", resetFun, enterEvent)
        ,EXIT("<Back")
    );

/////Main menu operations
result homeFun(){ 
    menuCMD= 'k'; 
    newCMD = true;
    DEBUG_PRINTLN("home fun");
    return proceed;
}

MENU(mainMenu,"YStruder v1.0",doNothing,noEvent,wrapStyle
        ,SUBMENU(pumpMenu)
        ,SUBMENU(extruderMenu)
        ,SUBMENU(settingsMenu)
        ,OP("Home", homeFun, enterEvent)
        ,OP("Pump", pumpFun, enterEvent)
);

////Outputs
MENU_OUTPUTS(out,MAX_DEPTH
        ,UCG_OUT(ucg,colors,fontX,fontY,DISPOFFSET_X,DISPOFFSET_Y,{0,0,UC_Width/fontX,UC_Height/fontY})
        ,NONE         //Must have at least two entries
        );

////Inputs
MENU_INPUTS(in,&encStream);
NAVROOT(nav,mainMenu,MAX_DEPTH,in,out); //the navigation root object

