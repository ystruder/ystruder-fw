#pragma once

#define ENCODER_DT      21
#define ENCODER_CLK     25
#define ENCODER_BTN     4
#define ENCODER_STEPS_PER_NOTCH    2   // Change this depending on which encoder is used
#define UC_CS   5      
#define UC_DC   26       
#define UC_RST  33       
