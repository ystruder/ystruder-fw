#pragma once

#define fontX 12
#define fontY 20
#define UC_Width 320
#define UC_Height 120
#define MAX_DEPTH 3
#define BLACK {0,0,0}
#define BLUE {0,0,255}
#define GRAY {128,128,128}
#define WHITE {255,255,255}
#define YELLOW {255,255,0}
#define RED {255,0,0}
#define GREEN {0,255,0}
#define DISPOFFSET_X 10
#define DISPOFFSET_Y 10

const colorDef<rgb> colors[] MEMMODE={
    {{BLACK,BLACK},{BLACK,BLUE,BLUE}},//bgColor
    {{GRAY,GRAY},{WHITE,WHITE,WHITE}},//fgColor
    {{WHITE,BLACK},{YELLOW,YELLOW,RED}},//valColor
    {{WHITE,BLACK},{WHITE,YELLOW,YELLOW}},//unitColor
    {{WHITE,GRAY},{BLACK,BLUE,WHITE}},//cursorColor
    {{WHITE,YELLOW},{BLACK,GREEN,GREEN}},//titleColor
};

