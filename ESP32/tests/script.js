//Based on: https://plnkr.co/edit/Imxwl9OQJuaMepLNy6ly?p=info
// by https://github.com/simonbrunel

var samplesToShow = 200;
var speed = 250;
var values = [];
var times = [];
var labels = [];
var charts = [];
var value = 0;

values.length = samplesToShow;
times.length = samplesToShow;
labels.length = samplesToShow;
values.fill(0);
labels.fill(0);
times.fill(0);

var Socket;
var samples = [];
var timestamps = [];
var minSamples;
var maxSamples;
var avgSamples;
var paused = false;

function convert2csv(args) {
    var result, ctr, keys, columnDelimiter, lineDelimiter, data;
    data = args.data || null;
    if (data == null || !data.length) {
        return null;
    }
    columnDelimiter = args.columnDelimiter || ',';
    lineDelimiter = args.lineDelimiter || '\n';
    result = 'timestamps';
    result += columnDelimiter;
    result += 'pressure';
    result += lineDelimiter;
    for (var i=0, tot = data[0].length; i < tot;i++){
        result += data[0][i];
        result += columnDelimiter;
        result += data[1][i];
        result += lineDelimiter;
    }
    return result;
}

function downloadCSV(args) {
    var data, filename, link;
    var exportData = [timestamps,samples];
    var csv = convert2csv({
        data: exportData
    });
    if (csv == null) return;
    filename = args.filename || 'export.csv';
    if (!csv.match(/^data:text\/csv/i)) {
        csv = 'data:text/csv;charset=utf-8,' + csv;
    }
    data = encodeURI(csv);
    link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();
}

function initSocket() {
    Socket = new WebSocket('ws://localhost:9001');
    //Socket = new WebSocket('ws://' + window.location.hostname + '/ws');
    Socket.onmessage = function(event){
        o = JSON.parse(event.data);
        window.pressure = parseFloat(o.s);
        window.timestamp = parseFloat(o.t)/5;       //convert timestamp into seconds
        window.power = parseFloat(o.p);
        samples.push(window.pressure)
        timestamps.push(window.timestamp)
        minSamples = Math.min.apply( null, samples);
        maxSamples = Math.max.apply( null, samples);
        if (samples.length)
        {
            sumSamples = samples.reduce(function(a, b) { return a + b; });
            avgSamples= Math.round((sumSamples / samples.length)*100)/100
        }
        pressurevalue.innerHTML = o.s
        pressureMax.innerHTML = maxSamples
        pressureAvg.innerHTML = avgSamples
        timestampvalue.innerHTML = o.t
        samplelen.innerHTML = samples.length
    }
}

function initialize() {
    charts.push(new Chart(document.getElementById("chart0"), {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                data: values,
                backgroundColor: 'rgba(255, 99, 132, 0.1)',
                borderColor: 'rgb(255, 99, 132)',
                borderWidth: 2,
                lineTension: 0,
                pointRadius: 0 
            }]
        },
        options: {
            responsive: true,
            animation: {
                duration: speed * 1.5,
                easing: 'linear'
            },
            legend: false,
            scales: {
                xAxes: [{
                    display: true,
                    ticks: {
                        maxRotation:0,
                        maxTicksLimit: 12,
                        autoSkip:true
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Time (s)'
                    },
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Pressure (bar)'
                    },
                    ticks: {
                        max: 10,
                        min: -10,
                        stepValue: 2
                    }
                }]
            }
        }
    }));

    document.getElementById('clearGraph').addEventListener('click', function() { 
        values.fill(0);
        charts.forEach(function(chart) { chart.update(); });
    });
    document.getElementById('clearData').addEventListener('click', function() { 
        samples = [];
        timestamps = [];
    });
    document.getElementById('pauseGraph').addEventListener('click', function() {paused =!paused;});
    document.getElementById('exportData').addEventListener('click', function() { downloadCSV({filename:"measurement-data.csv"});});
}

function advance() {
    values.push(window.pressure);
    values.shift();
    if (typeof window.timestamp != 'undefined'){
        labels.push(window.timestamp.toFixed(2));
        labels.shift();
    }
    if (!paused){
        charts.forEach(function(chart) { chart.update(); });
    }
    setTimeout(function() {
        requestAnimationFrame(advance);
    }, speed);
}

window.onload = function() {
    initSocket();
    initialize();
    advance();
};
