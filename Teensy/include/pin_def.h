#pragma once

#define DIR_PIN     14    
#define STEP_PIN    15    

#define EN_PIN      18    
#define CS_PIN      16     
#define DIAG1       21     

#define SCLK        9       //SCLK 6 and DOUT 5 in rev 2 
#define DOUT        10
#define PDWN        8
#define SPEED       7
#define GAIN0 5
#define GAIN1 6

#define UP          23
#define DOWN        22
#define STOP        20

#define EXT_STEP 3
#define EXT_DIR 4
