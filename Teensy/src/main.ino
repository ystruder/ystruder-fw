// LIBRARIES
#include <Arduino.h>
#include <ADS1232.h>
#include <EasyTransfer.h>
#include <TMC2130Stepper.h>
#include <AccelStepper.h>
#include <TMC2130Stepper_REGDEFS.h>
#include <TaskScheduler.h>
#include <EEPROM.h>

// HEADERS
#include "pin_def.h"
#include "DebugUtils.h"

// MACROS
#include "pin_def.h"
#define HWSERIAL Serial1
#define STALL_VALUE 14 // [-64..63]
/*#define DEBUG*/

// CLASSES
TMC2130Stepper driver = TMC2130Stepper(EN_PIN, DIR_PIN, STEP_PIN, CS_PIN);
AccelStepper stepper = AccelStepper(stepper.DRIVER, STEP_PIN, DIR_PIN);
IntervalTimer stepperTimer;
EasyTransfer ETin, ETout;
Scheduler runner;

// GLOBAL VARIABLES
volatile char state = 'i';               // i: init
volatile bool limit = false;
long targetPosition;
long targetSpeed;
int setPressure;
bool newTarget;
unsigned int sampleCount = 0;
bool extrusionState = false;
int cal_addr = 0;       // Location of calibration values in EEPROM

// Calibration data values to store
struct calibration {
    long offset;
    float scale;
};

struct RECEIVE_DATA_STRUCTURE {
    int targetSpeed;
    int targetPosition;
    int targetPressure;
    char cmd;
};

struct SEND_DATA_STRUCTURE {
    float force;
    long currentPosition;
    unsigned int samples;
    char state;
};

RECEIVE_DATA_STRUCTURE rxdata;
SEND_DATA_STRUCTURE txdata;
ADS1232 force = ADS1232(PDWN, SCLK, DOUT);

// INTERRUPTS
void runStepper()
{
    if (state == 'e') stepper.runSpeed();
    else stepper.run();
}

void stopstate()
{
    state = 'h';
}
void hitLimit()
{
    limit = true;           //Stall detection here?
}

void extrudeINT()
{
    extrusionState = digitalRead(EXT_STEP);
    DEBUG_PRINTLN(extrusionState ? "Extrusion start" : "Extrusion end");
}


bool home()
{
    DEBUG_PRINTLN("homing");
    limit = false;
    uint32_t drv_status = driver.DRV_STATUS();
    DEBUG_PRINTLN((drv_status & SG_RESULT_bm) >> SG_RESULT_bp, DEC);
    stepper.moveTo(-99999);
    if (limit) {
        DEBUG_PRINTLN("hit limit");
        stepper.setCurrentPosition(0);
        stepper.runToNewPosition(1000);
        state = 'i';
        return true;
    }
    else if (state != 'k') return true;
    else return false;
}

void driverSettings()
{
    stepper.setEnablePin(EN_PIN);
    stepper.disableOutputs();
    digitalWrite(CS_PIN, HIGH);
    driver.begin();                     // Initiate pins and registeries
    driver.push();                      // Reset registers
    driver.rms_current(900);            // Set stepper current to 900mA. With passive cooling this is the highest stable value. Add fan if you need to increase torque (i.e. thrust)
    driver.stealthChop(1);              // Enable extremely quiet stepping
    /*driver.stealth_autoscale(1);*/
    driver.toff(3);                     // Off time setting controls duration of slow decay phase.
    driver.tbl(2);                      // Comparator blank time of 24
    driver.hysteresis_start(4);         // Add 4 to hysteresis low value HEND 
    driver.hysteresis_end(-2);          // Hysteresis value which becomes used for the hysteresis chopper.
    driver.THIGH(0);                    // This velocity setting allows velocity dependent switching into a different chopper mode and fullstepping to maximize torque.
    driver.semin(5);                    // If the stallGuard2 result falls below sg_min*32, the motor current becomes increased to reduce motor load angle.
    driver.semax(2);                    // If the stallGuard2 result is equal to or above (sg_min+sg_max+1)*32, the motor current becomes decreased to save energy.
    driver.sedn(0b01);                  // Current increment steps per measured stallGuard2 value
    driver.sg_filter(1);                // Filtered mode, stallGuard2 signal updated for each four fullsteps (resp. six fullsteps for 3 phase motor) only to compensate for motor pole tolerances
    driver.diag1_stall(1);              // Stall pin 
    driver.diag1_active_high(1);        // Stall pin high = stalled
    driver.coolstep_min_speed(0xFFFFF);         // 20bit max
    driver.microsteps(16);
    driver.sg_stall_value(STALL_VALUE);     // Stallguard value
    stepper.setPinsInverted(false, false, true);
    stepper.setMaxSpeed(5000);              // 5000 mm/s @ 80 steps/mm
    stepper.setAcceleration(4000);          // 4000 mm/s^2
    stepper.setSpeed(2000);
    stepper.enableOutputs();
    stepperTimer.begin(runStepper, 100);  // run every 100us
}

void sendData()
{
    txdata.force = force.units_read(3);
    txdata.state = state;
    txdata.samples = sampleCount;
    sampleCount ++;
    txdata.currentPosition = stepper.currentPosition();
    ETout.sendData();
}

void receiveData()
{
    if (ETin.receiveData()) {
        targetPosition = rxdata.targetPosition;
        targetSpeed = rxdata.targetSpeed;
        setPressure = rxdata.targetPressure;
        state = rxdata.cmd;
    }
}

void setPins()
{
    pinMode(DOWN, INPUT_PULLUP);
    pinMode(UP, INPUT_PULLUP);
    pinMode(STOP, INPUT_PULLUP);
    pinMode(GAIN0, OUTPUT);
    pinMode(GAIN1, OUTPUT);
    pinMode(SPEED, OUTPUT);
    pinMode(CS_PIN, OUTPUT);
    pinMode(EXT_STEP, INPUT);
    pinMode(EXT_DIR, INPUT);
    digitalWrite(SPEED, LOW);
    digitalWrite(GAIN0, LOW);
    digitalWrite(GAIN1, LOW);
}

bool pumpDose(int pos, int speed)
{
    if (newTarget) {
        DEBUG_PRINTLN("pumping");
        stepper.move(pos);         //relative to currentPosition
        stepper.setMaxSpeed(speed);
        newTarget = false;
        DEBUG_PRINTLN(stepper.distanceToGo());
        return false;
    }
    else {
        if (stepper.distanceToGo() == 0) {
            return true;
        }
        else return false;
    }
}

void calibration_routine(){
    long new_offset = 0;
    long raw_read = 0;
    float new_scale = 0;
    force.OFFSET = 0;
    force.SCALE = 1.0;
    new_offset = force.raw_read(3);
    force.OFFSET = new_offset;
    DEBUG_PRINTLN("Place weight on load cell");
    delay(10000);           //Wait for 10 seconds for the calibration load to be placed
    raw_read = force.raw_read(3);
    new_scale= raw_read / 9.81;         //Using a calibration load of 1 kg  
    force.SCALE = new_scale;
    DEBUG_PRINTF("Calibration offset = %d\n", force.OFFSET);
    DEBUG_PRINTF("Calibration scale value =%f\n ", force.SCALE);
    DEBUG_PRINTF("Force = %f\n", force.units_read(3));
    calibration calvalues;
    calvalues.offset = force.OFFSET; 
    calvalues.scale = force.SCALE; 
    EEPROM.put(cal_addr, calvalues);
}

Task sendTask(100, TASK_FOREVER, &sendData);
Task receiveTask(100, TASK_FOREVER, &receiveData);

void setup()
{
    HWSERIAL.begin(115200);
    force.power_up();
    driverSettings();
    runner.init();
    runner.addTask(sendTask);
    sendTask.enable();
    runner.addTask(receiveTask);
    receiveTask.enable();
    setPins();
    calibration latestcalibration;
    EEPROM.get(cal_addr, latestcalibration);
    DEBUG_PRINTF("Calibration values = offset %d, scale %f \n",  latestcalibration.offset, latestcalibration.scale);
    force.OFFSET = latestcalibration.offset;     
    force.SCALE = latestcalibration.scale;
    ETin.begin(details(rxdata), &HWSERIAL);
    ETout.begin(details(txdata), &HWSERIAL);
    attachInterrupt(digitalPinToInterrupt(STOP), stopstate, CHANGE);
    attachInterrupt(digitalPinToInterrupt(DIAG1), hitLimit, CHANGE);
    attachInterrupt(digitalPinToInterrupt(EXT_STEP), extrudeINT, CHANGE);
#ifdef DEBUG
        Serial.begin(115200);
        while(!Serial);
#endif
    DEBUG_PRINTLN("Starting");
}

void loop()
{
    runner.execute();
    switch(state) {
    case 'i':
        newTarget = true;
        break;
    case 'c':           //Calibrate
        calibration_routine();
        state = 'i';
        break;
    case 'k':
        stepper.setMaxSpeed(5000);             
        stepper.setSpeed(5000);             
        while (!home());            //BLOCKING
        stepper.setSpeed(1000);             
        state = 'i';
        break;
    case 'h':
        DEBUG_PRINTLN("Stop");
        stepper.stop();
        while(!digitalRead(UP)){
            DEBUG_PRINTLN("UP");
            stepper.moveTo(stepper.currentPosition() - 400);
        }
        while(!digitalRead(DOWN)){
            DEBUG_PRINTLN("DOWN");
             stepper.moveTo(stepper.currentPosition() + 400);
            }
        break;
    case 'e':
        if (extrusionState ) {
            bool dir = digitalRead(EXT_DIR);
            DEBUG_PRINTLN(dir  ? "extrude" : "retract");
            if (dir) {
                stepper.setSpeed(rxdata.targetSpeed);             //This should be the speed from extrusion speed
            }
            else {
                stepper.setSpeed(-rxdata.targetSpeed);
            }
        }
        else stepper.setSpeed(0);
        break;
    case 'p':
        if (pumpDose(rxdata.targetPosition, rxdata.targetSpeed)) state = 'i';
        break;
    default:
        break;
        }
}
